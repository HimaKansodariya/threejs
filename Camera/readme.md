# Difference between PerspectiveCamera and OrthographicCamera in threeJS


## Fundamental Difference
A perspective camera is how we see the real word. If we take a look at the things around us, they have depth and we can judge their difference.
 
An Orthographic camera removes the sense of perspective, Object are drawn without perspective distortion. Distance of object from camera does not affect size.  


## Representation in ThreeJS

##### OrthographicCamera(left, right, top, bottom, near, far)
fov - camera frustum vertical field of view <br />
aspect - camera frustum aspect ratio <br />
near - camera frustum near plane <br />
far - camera frustum far plane  <br />

##### vPerspectiveCamera(fov, aspect, near, far)

left — Camera frustum left plane. <br />
right — Camera frustum right plane. <br />
top — Camera frustum top plane. <br />
bottom — Camera frustum bottom plane. <br />
near — Camera frustum near plane. <br />
far — Camera frustum far plane. <br />

###### Frustum – A frustum is a name of 3D shape that is like a pyramid with the tip sliced off.

## Features/Key Points

#### PerspectiveCamera
•	Need to simulate a real-world view <br />
•	Scene needs to have depth <br />
#### OrthographicCamera
•	Objects doesn’t need to have depth <br />
•	Objects are flat <br />


## When to Use

#### PerspectiveCamera

•	Widely used in 3D games <br />
•	When user want to create realistic illusion. <br />
•	When factor of depth is important. So, the object placed far will look smaller and near objects will look relative to them <br />

#### OrthographicCamera 

•	Widely used in 2D games <br />
•	When user need information at object level, not at observer level. <br />


## Comparison 

PerspectiveCamera give more information about depth and are often easier to view because you use perspective views in real life. OrthographicCamera make it much easier to compare two parts of the molecule, as there is no question about how the viewpoint may affect the perception of distance.

