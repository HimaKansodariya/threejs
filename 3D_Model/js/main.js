import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/build/three.module.js';
import {OrbitControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/controls/OrbitControls.js';
import {OBJLoader} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/loaders/OBJLoader.js';
import { GLTFLoader } from "https://cdn.jsdelivr.net/npm/three@0.121.1/examples/jsm/loaders/GLTFLoader.js";


function main(){

  // Fatch ID of canvas tag and render it
  const canvas = document.getElementById('model');
  const renderer = new THREE.WebGLRenderer({canvas});


  // PERSPECTIVE CAMERA
  //set patameter and set position of camera 
  // const fov = 45;
  // const aspect = 2; 
  // const near = 0.1;
  // const far = 100;
  // const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  // camera.position.set(0, 10, 20);


  // ORTHOGRAPHIC CAMERA
  //set patameter and set position of camera 
  const left = -20
  const right = 20
  const top = 50
  const bottom = -30 
  const near = 0.1
  const far = 100
  const camera = new THREE.OrthographicCamera(left, right, top, bottom, near, far);
  camera.position.set(0, 10, 20);
 

  const controls = new OrbitControls(camera, canvas);
  controls.target.set(0, 5, 0);
  controls.update();

  const scene = new THREE.Scene();
  scene.background = new THREE.Color('#F0FFF0');

  {
    const planeSize = 30;

    const loader = new THREE.TextureLoader();
    const texture = loader.load('https://threejsfundamentals.org/threejs/resources/images/checker.png');
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.magFilter = THREE.NearestFilter;
    const repeats = planeSize / 2;
    texture.repeat.set(repeats, repeats);

    const planeGeo = new THREE.PlaneGeometry(planeSize, planeSize);
    const planeMat = new THREE.MeshPhongMaterial({
      map: texture,
      side: THREE.DoubleSide,
    });
    const mesh = new THREE.Mesh(planeGeo, planeMat);
    mesh.rotation.x = Math.PI * -.5;
    scene.add(mesh);
  }
  
  // Load file of 3D object (.obj) using OBJLoader 
  {
    const objLoader = new OBJLoader();
    objLoader.load('./files/bugatti/bugatti.obj', (root) => {
      scene.add(root);
    });
  }
  

  function resizeRendererToDisplaySize(renderer) {
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {
      renderer.setSize(width, height, false);
    }
    return needResize;
  }

  function render() {

    if (resizeRendererToDisplaySize(renderer)) {
      const canvas = renderer.domElement;
      camera.aspect = canvas.clientWidth / canvas.clientHeight;
      camera.updateProjectionMatrix();
    }

    renderer.render(scene, camera);

    requestAnimationFrame(render);
  }

  requestAnimationFrame(render);

}

main();

