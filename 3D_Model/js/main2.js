import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/build/three.module.js';
import {OrbitControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/controls/OrbitControls.js';
import {OBJLoader} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/loaders/OBJLoader.js';
import { GLTFLoader } from "https://cdn.jsdelivr.net/npm/three@0.121.1/examples/jsm/loaders/GLTFLoader.js";
import {ObjectControls} from "./ObjectControls.js"

function main(){

  // Fatch ID of canvas tag and render it
  const canvas = document.getElementById('model');
  const renderer = new THREE.WebGLRenderer({canvas});

  // PERSPECTIVE CAMERA
  // set patameter and set position of camera 
  const fov = 80;
  const aspect = 2; 
  const near = 0.1;
  const far = 200;
  const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  camera.position.set(0, 10, 20);

  //set orbit control for camera 
  const controls = new OrbitControls(camera, canvas);
  controls.target.set(0, 5, 0);
  controls.update();

  // create scene and set proeprty 
  const scene1 = new THREE.Scene();
  scene1.background = new THREE.Color('#F0FFF0');

  // set Ambient light
  const color = 0xF00000;
  const intensity = 1;
  const light = new THREE.AmbientLight(color, intensity);
  scene1.add(light);
  
  //trying object control 
  const objcontrols = new ObjectControls(camera);

  // Load file of 3D object (.obj) using OBJLoader 
  {
    const objLoader = new OBJLoader();
    objLoader.load('./files/scifi-girl/scifi-girl.obj', function(object1){
      scene1.add(object1);
      object1.scale.set(-1,-1,-1);
    });
  }

  // set variable to animate indivdual object
  var targetPositionX = 2.435;

  // Load file of 3D object (.obj) using OBJLoader 
  {
    const objLoader1 = new OBJLoader();
    objLoader1.load('./files/bugatti/bugatti.obj', function(object2){
      scene1.add(object2);
      object2.rotation.z = -100

      //trying to animate object without moving other
      function loop(){
        renderer.render( scene1, camera );
        if (object2.position.x <= targetPositionX) {
          object2.position.x += 0.001; // 
        }
        requestAnimationFrame(loop);
      }
      loop();
    });  
  }

  // animate the scene 
  function animate() {
    requestAnimationFrame( animate );
    renderer.autoClear = true;
    controls.update();
    objcontrols.update();
    renderer.render( scene1, camera );
  }
  animate();
}

// call main function
main();

