import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/build/three.module.js';
import {OrbitControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/controls/OrbitControls.js';
import {TrackballControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/controls/TrackballControls.js'
import {OBJLoader} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/loaders/OBJLoader.js';
import { GLTFLoader } from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/loaders/GLTFLoader.js';
import {FBXLoader} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/loaders/FBXLoader.js';
import {TransformControls} from "https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/controls/TransformControls.js"
import { GUI } from "https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/libs/dat.gui.module.js"; 

var canvas = document.getElementById('model');
var canvasPosition = $(canvas).position();

var renderer = new THREE.WebGLRenderer({canvas, antialias:true});

// create scene and set proeprty 
var scene = new THREE.Scene();
scene.background = new THREE.Color('#ffe5b4');

var box = new THREE.Box3();

//Post Processing 
// const composer = new EffectComposer(renderer);
// composer.addPass(new RenderPass(scene, camera));
// composer.addPass(new EffectPass(camera, new DepthOfFieldEffect()));

// Camera- perspective
// var camera = perspectiveCamera(80, 2, 0.1, 200);
// camera.position.set(0, 10, 20);

// Camera- orthographic
var camera = orthographicCameraFourParam (100, 60, 1, 800);
camera.position.set(0, 0, 200);
scene.add(camera);

//set orbit control for camera 
var orbitControls = new OrbitControls(camera, canvas);
orbitControls.target.set(0, 5, 0);
orbitControls.rotateSpeed = 0.3;
orbitControls.enabled = false;
orbitControls.update();

// trackball controls 
var trackballControls = new TrackballControls(camera, canvas);
trackballControls.target.set(0,0,0);
trackballControls.rotateSpeed = 0.3; 
trackballControls.zoomSpeed = 0.5;
trackballControls.update();
trackballControls.enabled = true;

// Group of obejcts
var group = new THREE.Group();
group.name = 'myGroup';

// Loaders  
var objLoader = new OBJLoader();
var gltfLoader = new GLTFLoader();
var fbxLoader = new FBXLoader();

// Lights
var amibientLight = new THREE.AmbientLight(0xffffff, 1);
scene.add(amibientLight);

var directionalLight = new THREE.DirectionalLight(0xffffff, 1);
directionalLight.position.set(100,1000,100);
scene.add(directionalLight);

var pointLight = new THREE.PointLight(0xffffff ,0.5 ,0);
scene.add(pointLight);

var spotLight = new THREE.SpotLight(0xffffff , 0.5);
spotLight.position.set(100,1000,100);
scene.add(spotLight);


// RayCaster - to pick object with mouse
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

// GUI 
var gui = new GUI();


// -------------- TRANSFORM CONTROLS -------------------

var transformControls = new TransformControls(camera, renderer.domElement);
transformControls.addEventListener('change',render);
scene.add(transformControls);


// Keyboard event for transform controls 
window.addEventListener( 'keydown', function ( event ) {
    switch ( event.keyCode ) {
        case 87: // W key
            transformControls.setMode( "translate" );
            break;

        case 69: // E key
            transformControls.setMode( "rotate" );
            break;

        case 82: // R key
            transformControls.setMode( "scale" );
            break;
    }
});

// Buttons for transform Controls
$('#translate').click(function(){
    transformControls.setMode( "translate" );
});
$('#rotate').click(function(){
    transformControls.setMode( "rotate" );
});
$('#scale').click(function(){
    transformControls.setMode( "scale" );
});


//--------------------3D OBJECTS ----------------------

// CUBE - Geometric object 
var cube2 = new THREE.Object3D()
var geometryCube2 = new THREE.BoxGeometry( 5 , 5, 5 );
var materialCube2 = new THREE.MeshPhongMaterial( {color: 0xFFC0CB} );
cube2 = new THREE.Mesh( geometryCube2, materialCube2 );
group.add(cube2);
cube2.name = 'PARENT'; 
positionObject(cube2,10,2,5);
rotateObject(cube2,10,10,10);


// BOOK - GLTF obejct
var book, bookParent; 
bookParent = new THREE.Object3D();
var bookParentMat = new THREE.MeshBasicMaterial({color:0xffffff});
var bookParentGeo = new THREE.BoxGeometry (5,5,5); 
shivaChairParent = new THREE.Mesh(bookParentGeo, bookParentMat);
bookParent.name = 'PARENT';
gltfLoader.load('./3D_objects/book/book.gltf', function(object){
    book = object; 
    console.log('book.scene',book, book.scene);
    bookParent.add(book.scene);
    group.add(bookParent);
    // boundingBox(bookParent);
    book.name = 'BOOK'; 
    console.log('book position', book.scene.position);
    positionObject(bookParent,0,-10,-10);
    rotateObject(bookParent,10,100,10);
});

// DonatelloChair - FBX object
var donatelloChair, donatelloChairParent;
donatelloChairParent = new THREE.Object3D();
donatelloChairParent.name = 'PARENT'; 
fbxLoader.load('./3D_objects/DonatelloChair.fbx', function(object){
    donatelloChair = object; 
    donatelloChair.name = 'DonatelloChair';
    console.log('donatelloChair',donatelloChair);
    boundingBox(donatelloChair);
    donatelloChairParent.add(donatelloChair);
    positionObject(donatelloChair,0,0,0);
    group.add(donatelloChairParent);
    console.log('dona position', donatelloChair.position);
    positionObject(donatelloChairParent,-30,10,-50);
    // donatelloChair.children[0].material.color.setHex(0x13138c);
    donatelloChair.children[0].material.specular.setHex(0x42679d);
});

//ShivaChair - FBX obejct  
var shivaChair, shivaChairParent;
shivaChairParent = new THREE.Object3D(); 
shivaChairParent.name = "PARENT"; 
fbxLoader.load('./3D_objects/ShivaChair.fbx', function(object){
    shivaChair = object; 
    shivaChair.name = 'shivaChair';
    console.log('shivaChair',shivaChair);
    boundingBox(shivaChair);
    positionObject(shivaChair, 0,0,0); 
    shivaChairParent.add(shivaChair);
    group.add(shivaChairParent);
    console.log('shivaChair.position',shivaChair.position);
    positionObject(shivaChairParent,30,10,-40);
});

// Texure Loader and add image
var textureLoader = new THREE.TextureLoader();
var imageMaterial = new THREE.MeshPhongMaterial({map: textureLoader.load('../images/capture-camera.png'), opacity: 0.5 });
var imageGeometry = new THREE.PlaneGeometry(50,50);
var imageBlank = new THREE.Mesh(imageGeometry, imageMaterial);
// console.log('imageblank', imageBlank);
imageBlank.position.set(0,0,-100);
imageBlank.name = 'BURGER';
imageBlank.material.opacity = 0.3; 
imageBlank.material.transparent = true;
imageBlank.lookAt(camera.position);
// camera.add(imageBlank);



console.log('group',group);
scene.add(group);
console.log('scene childeren ', scene.children);

canvas.addEventListener( 'click', onMouseClick, false );
animate();
render();



// ------------------ MOUSE CLICK EVENT --------------------------
var finalIntersects = [];
function onMouseClick( event ) {
    console.log('you clicked')
    mouse.x = ((event.clientX - canvasPosition.left) / canvas.width) * 2 - 1.02;
    mouse.y = -((event.clientY - canvasPosition.top) / canvas.height) * 2 + 1.04;
    
	raycaster.setFromCamera( mouse, camera);
    console.log('scene.children',scene.children);
    // scene.add(new THREE.ArrowHelper(raycaster.ray.direction, raycaster.ray.origin, 300, 0xffffff,10,10) );
    
	var intersects = raycaster.intersectObjects(group.children, true);
    console.log('intersects',intersects)

    if(intersects.length>0){
        finalIntersects = []; 
        if(intersects[0].object.name == 'PARENT'){
            finalIntersects.push(intersects[ 0 ].object);    
        }
        else{
            findParent(intersects[0].object.parent);
        }
        console.log('finalIntersects',finalIntersects);
        transformControls.attach(finalIntersects[0]);
    }
    else{
       transformControls.detach();
    }
	renderer.render( scene, camera );
}

transformControls.addEventListener('mouseDown', function () {
    console.log('addEventListener mouseDown');
    trackballControls.enabled = false;
    orbitControls.enabled = false;
});
transformControls.addEventListener('mouseUp', function () {
    console.log('addEventListener mouseUp');
    trackballControls.enabled = true;
    orbitControls.enabled = true;
});


// ----------------- SCREENSHOT -----------------------

// Screenshot of canvas 
const elem = document.querySelector('#screenshot');
elem.addEventListener('click', () => {
    render();
  canvas.toBlob((blob) => {
    saveBlob(blob, `screencapture-${canvas.width}x${canvas.height}.png`);
  });
});
const saveBlob = (function() {
  const a = document.createElement('a');
  document.body.appendChild(a);
  a.style.display = 'none';
  return function saveData(blob, fileName) {
     const url = window.URL.createObjectURL(blob);
     a.href = url;
     a.download = fileName;
     a.click();   
  };  
}());


// capture selected part only 
var imageCanvas = document.getElementById('imageCanvas');
imageCanvas.height = 500;
imageCanvas.width = 300;

// Capture selected element .style. 
$("#capture").click(async function(){
    render();
    var dataURL = canvas.toDataURL('image/png');
    var image = new Image();
    image.src = dataURL;
    var ctx =await imageCanvas.getContext('2d');
    await ctx.drawImage(image, 100, 50 ,300, 500, 0,0, 300,500);
    alert('image captured! Hit "Dwonload" to download captured image'); 
});

$('#download').click(function(){
    imageCanvas.toBlob(async function(blob){ 
        var link = document.createElement("a");
        link.download = "image.png";
        link.href = window.URL.createObjectURL(blob);
        link.click();
    },'image/png');
});


// ------------------ FUNCTIONS -------------------------

// Function for scaling
function scaleObject(object, scaleValueX, scaleValueY, scaleValueZ){
    object.scale.set(scaleValueX,scaleValueY,scaleValueZ);
}

// Function to change position of obejct
function positionObject(object, positionValueX, positionValueY, positionValueZ){
    object.position.set(positionValueX, positionValueY, positionValueZ);
}

// Function to rotate object
function rotateObject(object, degreeX, degreeY, degreeZ){
    object.rotateX(THREE.Math.degToRad(degreeX));
    object.rotateY(THREE.Math.degToRad(degreeY));
    object.rotateZ(THREE.Math.degToRad(degreeZ));
}

// Perspective camera 
function perspectiveCamera(fov, aspect, near, far){
    var persCamera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    return persCamera;
}

// Orthographoc camera with six parameter 
function orthographicCameraSixParam(left, right, top, bottom, near, far){
    var orthoCamera = new THREE.OrthographicCamera(left, right, top, bottom, near, far);
    return orthoCamera; 
}

// OrthographicCamera with four parameter 
function orthographicCameraFourParam(width, height, near, far){
    var orthoCamera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, near, far );
    return orthoCamera;
}

// render function 
function render(){
    renderer.render( scene, camera );
    // if(cube2){
    //     cube2.rotation.x += 0.1;
    // }
    // if(shivaChair){
    //     shivaChair.rotation.y += 0.01; 
    // }
    var cameraPosition = camera.position;
    // imageBlank.position.set( cameraPosition.x, cameraPosition.y);
    // console.log('rendered'); 
}

// Function to animate object
function animate() {
    requestAnimationFrame( animate );
    // orbitControls.update();
    trackballControls.update();
    // renderer.render( scene, camera );
    render();
}

function findParent (parent){
    if(parent.name == 'PARENT'){
        finalIntersects.push(parent);
    }
    else{
        findParent(parent.parent);
    }
}

function boundingBox(object){
    var center = new THREE.Vector3();   
    object.children[0].geometry.computeBoundingBox();
    object.children[0].geometry.boundingBox.getCenter(center);
    object.children[0].geometry.center();
    object.position.copy(center);
}


