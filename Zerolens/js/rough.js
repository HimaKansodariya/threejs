
// ORTHOGRAPHIC CAMERA
const width = 40, height =40; 
const camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 1000 );
camera.position.set(0, 10, 20);

import * as THREE from "./threeJsMaster/build/threejs.module.js"; 
import {OrbitControls} from './threeJsMaster/examples/jsm/controls/OrbitControls.js';
import {OBJLoader} from './threeJsMaster/examples/jsm/loaders/OBJLoader.js';
import { GLTFLoader } from "./threeJsMaster/examples/jsm/loaders/GLTFLoader.js";

import { DepthOfFieldEffect } from "./postprocessing-main/src/effects/DepthOfFieldEffect.js";
import { EffectComposer } from './threeJsMaster/examples/jsm/postprocessing/EffectComposer.js';
import { EffectPass } from './postprocessing-main/src/passes/EffectPass.js';
import { RenderPass } from './postprocessing-main/src/passes/RenderPass.js';

import {EffectComposer} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/postprocessing/EffectComposer.js';
import {RenderPass} from 'https://threejsfundamentals.org/threejs/resources/threejs/r127/examples/jsm/postprocessing/RenderPass.js';
import { EffectComposer, EffectPass, RenderPass } from "postprocessing";


// scifiGirl - .obj Object
var scifiGirl;
objLoader.load('./3D_objects/scifi-girl/scifi-girl.obj', function(object){
    scifiGirl = object;
    console.log('scifi girl',scifiGirl);
    group.add(scifiGirl);
    scifiGirl.name = 'scifiGirl';
    positionObject(shivaChair,-10,10,-45);
});  

// BUGATTI - .obj Object 
var bugatti;
objLoader.load('./3D_objects/bugatti/bugatti.obj', function(object){ 
    bugatti = object;
    console.log('bugatti',bugatti);
    group.add(bugatti);
    bugatti.name = 'Bugatti';
});  

// ----------------- CUBE -----------------
const geometryCube = new THREE.BoxGeometry( 5, 5, 5 );
const materialCube = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
const cube = new THREE.Mesh( geometryCube, materialCube );
// scene1.add( cube );
// positionObject(cube,5,10,5);

// -------------- SPHERE ----------------------------
const geometrySphere = new THREE.SphereGeometry(3, 50, 50, 0, Math.PI * 2, 0, Math.PI * 2);
const materialSphere = new THREE.MeshBasicMaterial( {color: 0xff6600} );
const sphere = new THREE.Mesh(geometrySphere, materialSphere);
// scene1.add(sphere);
// positionObject(sphere,5,-1,2);

// -------------- SPHERE ----------------------------
const geometrySphere2 = new THREE.SphereGeometry(4, 60, 60, 0, Math.PI * 2, 0, Math.PI * 2);
const materialSphere2 = new THREE.MeshBasicMaterial( {color: 0xFFC0CB} );
const sphere2 = new THREE.Mesh(geometrySphere2, materialSphere2);
// scene1.add(sphere2);
// positionObject(sphere2,-5,8,5);

//----------- HEART -------------------

const x=0, y=0;
const heartShape = new THREE.Shape();
heartShape.bezierCurveTo( x + 5, y + 5, x + 4, y, x, y );
heartShape.bezierCurveTo( x - 6, y, x - 6, y + 7,x - 6, y + 7 );
heartShape.bezierCurveTo( x - 6, y + 11, x - 3, y + 15.4, x + 5, y + 19 );
heartShape.bezierCurveTo( x + 12, y + 15.4, x + 16, y + 11, x + 16, y + 7 );
heartShape.bezierCurveTo( x + 16, y + 7, x + 16, y, x + 10, y );
heartShape.bezierCurveTo( x + 7, y, x + 5, y + 5, x + 5, y + 5 );

const geometryHeart = new THREE.ShapeGeometry( heartShape );
const materialHeart = new THREE.MeshBasicMaterial( { color: 0x9370DB } );
const heart = new THREE.Mesh( geometryHeart, materialHeart ) ;
scene1.add( heart );
positionObject(heart,0,0,0);

// ------------ STUDIO OFFICE --------------------
gltfLoader.load('./3D_objects/studio_office_interior/scene.gltf', function(object){
    scene1.add(object.scene);
});



var shivaChairDuplicate = new THREE.Object3D();

    for(let i = 0; i < group.children.length; i++){
        console.log('inside for')
        if(group.children[i].children[0].geometry == dragArray[0].geometry){
            console.log('inside if')
            var shivaChairDuplicateGeometry = dragArray[0].geometry; 
            var shivaChairDuplicateMaterial = dragArray[0].material;
            shivaChairDuplicate = new THREE.Mesh(shivaChairDuplicateGeometry,shivaChairDuplicateMaterial);
            scene1.add(shivaChairDuplicate);
            // scene1.add( group);
            positionObject(shivaChairDuplicate, group.children[i].position.x, group.children[i].position.y, group.children[i].position.z);
            // group.remove(group.children[i]);
            console.log('group inside if', group);
          


        }
    }


    // dat GUI for donatellochair 
    var meshPhongMaterialFolder = gui.addFolder('THREE.MeshPhongMaterial');
    var data = {
        color: donatelloChair.children[0].material.color.getHex(),
        emissive: donatelloChair.children[0].material.emissive.getHex(),
        specular: donatelloChair.children[0].material.specular.getHex()
    };
    meshPhongMaterialFolder.addColor(data, 'color').onChange(() => { donatelloChair.children[0].material.color.setHex(Number(data.color.toString().replace('#', '0x'))) });
    meshPhongMaterialFolder.addColor(data, 'emissive').onChange(() => { donatelloChair.children[0].material.emissive.setHex(Number(data.emissive.toString().replace('#', '0x'))) });
    meshPhongMaterialFolder.addColor(data, 'specular').onChange(() => { donatelloChair.children[0].material.specular.setHex(Number(data.specular.toString().replace('#', '0x'))) });
    meshPhongMaterialFolder.add(donatelloChair.children[0].material, 'shininess', 0, 1024);
    meshPhongMaterialFolder.add(donatelloChair.children[0].material, 'reflectivity', 0, 1);
    meshPhongMaterialFolder.add(donatelloChair.children[0].material, 'wireframe');
    meshPhongMaterialFolder.add(donatelloChair.children[0].material, 'wireframeLinewidth', 0, 10);
    meshPhongMaterialFolder.add(donatelloChair.children[0].material, 'flatShading').onChange(() => updateMaterial())
    meshPhongMaterialFolder.add(donatelloChair.children[0].material, 'refractionRatio', 0, 1);
    meshPhongMaterialFolder.open();


    
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;


    // GIVES BLANK WHEN OBEJCT IS ROTATING IN CANVAS 
$('#screenshot').click(function(){
    console.log('clicked')
    canvas.toBlob(async function(blob){
        render();
        var link = document.createElement("a");
        console.log('link created')
        link.download = "image.png";
        link.href = window.URL.createObjectURL(blob);
        link.click();
        console.log( 'link clciked');
    },'image/png');
});



if(intersects.length>0){
    trackballControls.enabled = false;
    dragArray = []; 
    

    if(intersects.length == 1){
        dragArray.push(intersects[ 0 ].object);    
    }
    else{
        
        dragArray.push(findParent(intersects[0].object.parent));
        console.log('xyz',xyz);
        if(intersects[ 0 ].object.parent.parent.name == 'myGroup'|| intersects[ 0 ].object.parent.name == 'myGroup'|| intersects[ 0 ].object.parent.parent.type == 'Scene'){
            console.log('intersects[0].parent',intersects[ 0 ].object.parent);
            dragArray.push(intersects[ 0 ].object.parent);  
        }
        else{
            console.log('intersects[0].parent',intersects[ 0 ].object.parent.parent);
            dragArray.push(intersects[ 0 ].object.parent.parent);  
        }   
    }
    console.log('dragarray',dragArray);
    transformControls.attach(dragArray[0]);
}


var shivaChairParentMaterial = new THREE.MeshBasicMaterial({color:0xffffff});
var shivaChairParentGeo = new THREE.BoxGeometry (5,5,5); 
shivaChairParent = new THREE.Mesh(shivaChairParentGeo, shivaChairParentMaterial);

scene.children[0].children[0].children[0].children[1].children[0].geometry
scenes[0].children[0].children[0].children[0].children[2].children[0].geometry